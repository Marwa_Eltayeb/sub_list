package com.example.marwa.sub_list;

/**
 * Created by Marwa on 8/6/2018.
 */

public class List_item {
    private String Title;
    private String folder_id;
    public List_item(String title, String folder_id) {
        Title = title;
        this.folder_id = folder_id;
    }
    public String getTitle() {
        return Title;
    }
    public void setTitle(String title) {
        Title = title;
    }
    public String getFolder_id() {
        return folder_id;
    }
    public void setFolder_id(String folder_id) {
        this.folder_id = folder_id;
    }
}