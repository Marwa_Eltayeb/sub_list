package com.example.marwa.sub_list;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

public class Web_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        Intent intent = getIntent();
        String link = intent.getStringExtra("page");
        WebView webView = (WebView) findViewById(R.id.webView);
        webView.loadUrl("file:///android_asset/"+link);
    }



}


