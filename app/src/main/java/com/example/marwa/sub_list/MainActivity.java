package com.example.marwa.sub_list;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    ArrayList<List_item> list_items = new ArrayList<>();
    String list_type = "main_index";
    String book_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        Index("index.txt");

    }


    @Override
    public void onBackPressed() {
        if (list_type.equals("sub_index")) {
            Index("index.txt");
            list_type = "main_index";
        } else {
            super.onBackPressed();
        }
    }

    public void Index(String book_id) {
        list_items.clear();
        try {
            InputStream inputStream = getAssets().open(book_id);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            int id = 0;
            while ((line = bufferedReader.readLine()) != null) {
                id++;
                list_items.add(new List_item(line, "book_" + id));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ListAdapter adapter = new ListAdapter(list_items);
        listView.setAdapter(adapter);

    }



    private class ListAdapter extends BaseAdapter {
        ArrayList<List_item> list = new ArrayList<>();
        ListAdapter(ArrayList<List_item> list) {
            this.list = list;
        }
        @Override
        public int getCount() {
            return list.size();
        }
        @Override
        public Object getItem(int i) {
            return list.get(i);
        }
        @Override
        public long getItemId(int i) {
            return i;
        }
        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {

            View view1 = View.inflate(getApplicationContext(), R.layout.row_item, null);
            TextView TextTitle = (TextView) view1.findViewById(R.id.textView_row_itme_title);
            TextTitle.setText(list.get(i).getTitle());
            TextTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (list_type.equals("main_index")) {
                        list_type = "sub_index";
                        book_id = list.get(i).getFolder_id();
                        Index(list.get(i).getFolder_id() + "/index.txt");
                    }else if (list_type.equals("sub_index")) {
                        Intent intent = new Intent(MainActivity.this, Web_Activity.class);
                        intent.putExtra("page", book_id+"/html/page" +i+".html");
                        startActivity(intent);
                    }

                }
            });
            return view1;
        }
    }


}
